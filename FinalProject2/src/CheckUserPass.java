

//public static String correctUser = UserPassGUI.savedUser;
//public static String correctPass = UserPassGUI.savedPass;
import java.awt.event.ActionEvent;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class CheckUserPass extends JFrame{
	private JPanel jp1 = new JPanel();
	private JPanel jp2 = new JPanel();
	private JPanel jp3 = new JPanel();
	private JLabel heading;
	private JLabel user;
	private JLabel pass;
	private JTextField userInput = new JTextField(20);
	private JPasswordField userPass = new JPasswordField(20);
	private JButton doneButton = new JButton("Login");	

	public CheckUserPass()
	{
		System.out.println(UserPassGUI.savedUser);
		setLayout(new FlowLayout());
		heading = new JLabel("Login Please");
		jp1.add(heading);
		add(jp1);
		
		setLayout(new FlowLayout());
		user = new JLabel("Username");
		jp2.add(user);
		jp2.add(userInput);
		add(jp2);
		
		setLayout(new FlowLayout());
		pass = new JLabel("Password");
		jp3.add(pass);
		jp3.add(userPass);
		add(jp3);
		
		doneButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String authUser = userInput.getText();
				String authPass = userPass.getText();
				boolean authCheck = (authUser.equals(UserPassGUI.savedUser)&&
						authPass.equals(UserPassGUI.savedPass));
				
				if (authCheck)
				{
					
					setVisible(false);
				}
				else 
					System.out.println("no workie, try again");
			}
		}
				);
		jp3.add(doneButton);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(400,400);
		setVisible(false);
	}

}